#include "main.h"

#include "lexer.h"
#include "token_buffer.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

void process(std::string const& path)
{
    std::cout << path << std::endl;

    std::ifstream input(path, std::ios::in);

    if (!input)
    {
        std::cerr << "Failed to open '" << path << "'." << std::endl;
        return;
    }

    Lexer lexer(input);
    TokenBuffer token_buffer(lexer);

    std::cout << "TOKEN\tTEXT" << std::endl;
    
    for(std::size_t n = 0;;n++)
    {
        LexicalToken const& token = token_buffer.peek(n);

		std::cout
#ifdef NDEBUG
			<< (int) token.id()
#else
            << token_name(token.id())
#endif
            << "\t" << token.text()
            << std::endl;

        if (LexicalToken::INVALID_TOKEN == token)
        {
            break;
        }
    }
}

int main(int argc, char *argv[])
{
    std::vector<std::string> args(argv + 1, argv + argc);

    for (auto arg : args)
    {
        process(arg);
    }
}
