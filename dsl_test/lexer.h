#pragma once

#include <istream>

#include "lexical_token.h"
#include "token_provider.h"

class Lexer : public ITokenProvider
{
public:
    explicit Lexer(std::istream& path);

    bool next() override;
    LexicalToken const& token() const override;

private:
    bool accept_single_char_tokens(std::istream::traits_type::char_type peek_value);
    bool accept_number(std::istream::traits_type::char_type peek_value);
    bool accept_symbol(std::istream::traits_type::char_type peek_value);

private:
    std::istream& _input;
    LexicalToken _token;
    std::string _text;

    static std::string const SINGLE_CHAR_TOKENS;
};

#ifndef NDEBUG
inline char const* token_name(LexicalToken::Token token_id)
{
    if (false)
        ;
#define X_TOKEN(arg) else if(token_id == LexicalToken::Token::arg) return #arg;
#include "tokens.inc"
    return "";
}
#endif

inline LexicalToken::Token character_token(char ch)
{
    char token_character = 0;
#define X_TOKEN(token_id) if(token_character == ch) return LexicalToken::Token::token_id;
#define X_SINGLE_CHARACTER(arg) token_character = arg;
#include "tokens.inc"

    return LexicalToken::Token::INVALID;
}
