#pragma once

class LexicalToken;

class ITokenProvider
{
public:
    virtual ~ITokenProvider() {}
    virtual bool next() = 0;
    virtual LexicalToken const& token() const = 0;
};