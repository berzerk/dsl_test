#include "lexer.h"

#include <string>
#include <iostream>
#include <vector>

std::string const Lexer::SINGLE_CHAR_TOKENS = "()[]{};";
Lexer::Lexer(std::istream& input)
    : _input(input)
    , _token(LexicalToken::Token::INVALID)
{
}

bool Lexer::next()
{
    if (!this->_input.good())
    {
        std::cerr << "bad" << std::endl;
        this->_token = LexicalToken(LexicalToken::Token::INVALID);
        return false;
    }

    // eat whitespace
    while(isspace(this->_input.peek()))
    {
        this->_input.get();
    }

    // handle eof as any other token
    // but if this function is called again, return false
    if (!std::istream::traits_type::not_eof(this->_input.peek()))
    {
        this->_token = LexicalToken(LexicalToken::Token::EOF_TOKEN);
        return true;
    }

    // check what the next character is
    std::istream::traits_type::char_type peek_value = std::istream::traits_type::to_char_type(this->_input.peek());

    if (this->accept_single_char_tokens(peek_value)
        || this->accept_number(peek_value)
        || this->accept_symbol(peek_value)
        )
    {
        return true;
    }

    
    this->_token = LexicalToken::INVALID_TOKEN;
    return false;
}

bool Lexer::accept_single_char_tokens(std::istream::traits_type::char_type peek_value)
{
    // single character tokens
    if (std::string::npos != Lexer::SINGLE_CHAR_TOKENS.find(peek_value))
    {
        this->_input.get();
        this->_token = LexicalToken(
            character_token(static_cast<char>(peek_value)),
            std::string("") + static_cast<char>(peek_value));
        return true;
    }

    return false;
}

bool Lexer::accept_number(std::istream::traits_type::char_type peek_value)
{
    if (isdigit(peek_value) || peek_value == '.') {
        std::vector<char> number;
        size_t count = 0;
        do {
            count++;
            number.push_back(peek_value);
            _input.get();
            peek_value = std::istream::traits_type::to_char_type(this->_input.peek());
        } while (isdigit(peek_value) || peek_value == '.');

        this->_token = LexicalToken(
            LexicalToken::Token::NUMBER,
            std::string(&number[0], number.size()));

        return true;
    }

    return false;
}

bool Lexer::accept_symbol(std::istream::traits_type::char_type peek_value)
{
    if (isalpha(peek_value) || '_' == peek_value)
    {
        std::vector<char> symbol;
        size_t count = 0;
        do
        {
            count++;
            symbol.push_back(peek_value);
            _input.get();
            peek_value = std::istream::traits_type::to_char_type(this->_input.peek());
        } while (isalpha(peek_value) || '_' == peek_value);

        this->_token = LexicalToken(
            LexicalToken::Token::SYMBOL,
            std::string(&symbol[0], symbol.size()));

        return true;
    }

    return false;
}

LexicalToken const& Lexer::token() const
{
    return this->_token;
}
