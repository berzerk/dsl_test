#include "token_buffer.h"

#include "lexical_token.h"
#include <iostream>

TokenBuffer::TokenBuffer(ITokenProvider& provider)    
    : _default_token(LexicalToken::INVALID_TOKEN)
    , _provider(provider)
{
}

LexicalToken const& TokenBuffer::peek(std::size_t pos)
{
    while (pos >= this->_buffer.size() && this->_provider.next())
    {
        LexicalToken const& token = this->_provider.token();
        this->_buffer.push_back(token);
    }

    if (pos >= this->_buffer.size())
    {
        std::cout << "out of tokens." << std::endl;
        return _default_token;
    }

    return this->_buffer[pos];
}