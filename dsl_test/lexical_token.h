#pragma once
#include <string>

class LexicalToken
{
public:
    enum class Token {
#define X_TOKEN(arg) arg,
#include "tokens.inc"
    };

    LexicalToken(Token token, std::string text)
        : _id(token)
        , _text(text)
    {
    }

    explicit LexicalToken(Token token) : LexicalToken(token, "") {}

    bool operator==(const LexicalToken &rhs) const { return this->_id == rhs._id; }

    Token id() const { return this->_id; }
    std::string const& text() const { return this->_text; }

    static LexicalToken const& INVALID_TOKEN;

private:
    Token _id;
    std::string _text;
};
