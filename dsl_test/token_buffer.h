#pragma once

#include <vector>
#include "token_provider.h"

class LexicalToken;

class TokenBuffer
{
public:
    explicit TokenBuffer(ITokenProvider& provider);
    LexicalToken const& peek(std::size_t pos = 0);

private:
    std::vector<LexicalToken> _buffer;
    LexicalToken const& _default_token;
    ITokenProvider& _provider;
};